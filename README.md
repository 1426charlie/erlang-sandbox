# Erlang Sandbox

```
 ___     _                  ___               _ _             
| __|_ _| |__ _ _ _  __ _  / __| __ _ _ _  __| | |__  _____ __
| _|| '_| / _` | ' \/ _` | \__ \/ _` | ' \/ _` | '_ \/ _ \ \ /
|___|_| |_\__,_|_||_\__, | |___/\__,_|_||_\__,_|_.__/\___/_\_\
                    |___/                                     
```

## Alarm / Alert Trigger System

Create an environment that starts a list of services to monitor rooms.  When a condition is met, the alarm will trigger.  Prepared alarms invoke a series of actions sequencially or in parallel or any combination.

## Install on Mac

```bash
brew install erlang
```


###### darryl.west | 2018.04.18
