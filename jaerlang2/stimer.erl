%% ---
%%  Excerpted from "Programming Erlang, Second Edition",
%%  Visit http://www.pragmaticprogrammer.com/titles/jaerlang2 for more book information.
%%  Creates a cancleable timer to execute a fun on timeout then exit; 
%%---

-module(stimer).
-export([start/2, cancel/1]).

%% time in milliseconds...
start(Time, Fun) -> spawn(fun() -> timer(Time, Fun) end).
cancel(Pid) -> Pid ! cancel.

timer(Time, Fun) ->
    receive
	cancel ->
	    void
    after Time ->
	    Fun()
    end.

