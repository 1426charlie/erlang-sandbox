%%
%% a template for concurrent modules
%% 2018-04-18 10:17:35
%%

-module(ctemplate).
-compile(export_all).
%% -export([start/0,rpc/2]).

start() ->
    spawn(?MODULE, loop, []).

rpc(Pid, Request) ->
    Pid ! {self(), Request},
    receive
        {Pid, Response} ->
            Response
    end.

loop() ->
    receive
        Any ->
            io:format("Received:~p~n", [Any]),
            loop()
    end.

