%%
%% USE: clock:start(5000, fun() -> io:format("TICK ~p~n",[erlang:now()]) end).
%%

-module(clock).
-export([run/0,start/2,stop/0]).

run() ->
    start(1000, fun() -> io:format("tick ~p~n", [erlang:now()]) end).

start(Time, Fun) ->
    register(clock, spawn(fun() -> tick(Time, Fun) end)).

stop() -> clock ! stop.

tick(Time, Fun) ->
    receive
        stop ->
            void
    after Time ->
        Fun(),
        tick(Time, Fun)
    end.

