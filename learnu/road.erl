%%
%% fastest route
%% 2018-04-02 10:29:38
%%

-module(road).
-export([main/0]).

main() ->
    Filename = "road.txt",
    {ok, Binary} = file:read_file(File),
    parse_map(Binary).

parse_map(Bin) when is_binary(Bin) ->
    parse_map(binary_to_list(Bin));
parse_map(Str) when is_list(Str) ->
    Values = [list_to_integer(X) || X <- string:tokens(Str, "\r\n\t ")],
    group_vals(Values, []).

group_vals([], Acc) ->
    lists:reverse(Acc);
group_vals([A,B,X|Rest], Acc) ->
    group_vals(Rest, [{A,B,X} | Acc]).


