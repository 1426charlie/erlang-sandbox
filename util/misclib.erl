%%
%% collection of utilities
%% 2018-04-01 13:19:33
%%

-module(misclib).
-export([odd_even/1, inc/1, dec/1, incr/1, decr/1, map/2]).

odd_even(L) ->
    odd_even_acc(lists:reverse(L), [], []).

odd_even_acc([H|T], Odds, Evens) ->
    case (H rem 2) of
        1 -> odd_even_acc(T, [H|Odds], Evens);
        0 -> odd_even_acc(T, Odds, [H|Evens])
    end;
    odd_even_acc([], Odds, Evens) ->
        {Odds, Evens}.

inc([]) -> [];
inc([H|T]) -> [H + 1|inc(T)].

dec([]) -> [];
dec([H|T]) -> [H + 1|dec(T)].

incr(X) -> X + 1.
decr(X) -> X - 1.

map(_, []) -> [];
map(F, [H|T]) -> [F(H) | map(F, T)].

%% use: misclib:map(fun misclib:incr/1, lists:seq(1,20)).
