%%
%% classic fibonacci with iteration; numbers are in list are kept in reverse order for efficiency
%% 2018-04-01 11:08:49
%%

-module(fib).
-include_lib("eunit/include/eunit.hrl").

-export([ fib/1 ]).
-export([ tc/1, tc_big/0 ]).

%% note the list is in reverse order...
fib(N) -> fib(N, 0, 1).

fib(0, Result, _Next) -> Result;
fib(Iter, Result, Next) -> fib(Iter-1, Next, Result+Next).

tc(N) -> timer:tc(fib, fib, [ N ]).

tc_big() -> 
    io:write(tc(1024)),
    io:nl().

fib_test_() ->
   [?_assert(fib(0) =:= 1),
    ?_assert(fib(1) =:= 1),
    ?_assert(fib(2) =:= 2),
    ?_assert(fib(3) =:= 3),
    ?_assert(fib(4) =:= 5),
    ?_assert(fib(5) =:= 8),
    ?_assertException(error, function_clause, fib(-1)),
    ?_assert(fib(31) =:= 2178309)
   ].
