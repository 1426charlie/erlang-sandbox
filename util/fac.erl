%%
%% classic factorial with lists and tail recursion;
%% 2018-04-01 11:08:49
%%

-module(fac).
-export([ fac/1, fact/1 ]).
-export([ tc/1, tct/1 ]).

%% note the list is in reverse order...
fac(N) -> lists:foldl(fun(X,Y) -> X*Y end, 1, lists:seq(1,N)).

fact(N) -> fact(N-1,N).
fact(1,N) -> N;
fact(I,N) -> fact(I-1,N*I).

tc(N) -> timer:tc(fac, fac, [ N ]).
tct(N) -> timer:tc(fac, fact, [ N ]).
