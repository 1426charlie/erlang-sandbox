%% created by dpw on 2018-03-17 12:27:14

-module(fizzbuzz).
-export([go/0,show/0]).

go() ->
    F = fun(N) when N rem 15 == 0 -> integer_to_list(N) ++ "FizzBuzz";
           (N) when N rem 3 == 0 -> integer_to_list(N) ++ "Fizz";
           (N) when N rem 5 == 0 -> integer_to_list(N) ++ "Buzz";
           (N) -> integer_to_list(N)
        end,
    [ F(N) || N <- lists:seq(1,100)] .

show() -> show(go()).
show([]) -> io:format("~n", []);
show(List) -> io:format("~s~n", [ hd(List) ]), show(tl(List)).
