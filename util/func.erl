%%
%% functions
%% 2018-04-01 13:19:33
%%
%% http://learnyousomeerlang.com/higher-order-functions

-module(func).
-export([aaa/0, bbb/1]).
-export([prepareAlarm/1]).
-export([sum/1]).
-export([even/1]).
-export([odd/1]).

%% return a closure: 
aaa() ->
    Secret = "pony",
    fun() -> Secret end.

bbb(F) -> "aaa's secret word is " ++ F().

%% arm the alarm for a specific room; return the alarm-trip funcion
%%
%% use: 
%%  Alert = func:prepareAlarm("living room is on fire").
%%  Alert(). 
%%
prepareAlarm(Room) -> 
    io:format("Alarm set in ~s.~n", [Room]), 
    fun() -> io:format("Alarm triggered: ~s!~n", [Room]) end .

%% find the sum
sum(List) -> sum(List, 0).
sum([], Sum) -> Sum;
sum([H|T], Sum) -> sum(T, H + Sum).

even(X) -> X rem 2 == 0.
odd(X) -> X rem 2 == 1.
    
