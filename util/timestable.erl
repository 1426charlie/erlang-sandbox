%% @doc This is a module version of the escript code
%% 
%% @ref: https://github.com/mrallen1/erlang-n-go
%% 
%% Start erlang shell, compile and load it by typing
%% `c(timestable)'
%% `timestable:main()'
%% @end

-module(timestable).
-export([show/0,main/1]).
-define(SIZE, 12).

timestable(N, SendPid) ->
    compute_times(N, ?SIZE, SendPid).

compute_times(_X, 0, _Pid) ->
    ok;
compute_times(X, I, Pid) ->
    Pid ! {result, X, I, X*I, self()},
    compute_times(X, I-1, Pid).

show() ->
    {T, _} = timer:tc(timestable, main, [ ?SIZE ]),
    io:fwrite("~w microseconds~n", [ T ]).

main(N) ->
    spawn_workers(N, self()),
    listen(0, N*?SIZE).

listen(R, R) ->
    ok;
listen(R, Max) ->
    NewR = receive
        {result, X, I, Quot, From} ->
            io:format("~w x ~w = ~w (~w)~n", [X, I, Quot, From]),
            R + 1
    end,
    listen(NewR, Max).

spawn_workers(0, _SendPid) ->
    ok;
spawn_workers(N, SendPid) ->
    spawn(fun() -> timestable(N, SendPid) end),
    spawn_workers(N-1, SendPid).
