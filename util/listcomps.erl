%%
%%
%% @created 2018-04-05 09:09:19
%%

-module(listcomps).

-export([createEvenList/1,createOddList/1]).
-export([createCityList/0, getFoggy/1, getSunny/1]).

%% the easy/obvious comps
createEvenList(L) -> [ X || X <- L, X rem 2 == 0 ].
createOddList(L) -> [ X || X <- L, X rem 2 /= 0 ].

%% a bit more creative
createCityList() -> [ {london, fog}, {seattle, rain}, {losangles, sun}, {pheonix, sun}, {sanfrancisco, fog} ].
getFoggy(L) -> [ C || {C, fog} <- L ].
getSunny(L) -> [ C || {C, sun} <- L ].

%% return tuples
